package com.example.product;



import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProductController {

	 @Autowired
	    private ProductService ps;
	 
	 @RequestMapping("/")
	 public ModelAndView home() {
	     List<Product> pList = ps.listAll();
	     ModelAndView mav = new ModelAndView("index");
	     mav.addObject("listProducts", pList);
	     return mav;
	 }
	 @RequestMapping("/new")
	 public String newProduct(Map<String, Object> model) {
	     Product p = new Product();
	     model.put("product", p);
	     return "new_product";
	 }
	 @RequestMapping(value = "/save", method = RequestMethod.POST)
	 public String saveProduct(@ModelAttribute("product") Product p) {
	     ps.save(p);
	     return "redirect:/";
	 }
	 @RequestMapping("/edit")
	 public ModelAndView editProductDetails(@RequestParam int id) {
	     ModelAndView mav = new ModelAndView("edit_product");
	     Product p = ps.get(id);
	     mav.addObject("product", p);	  
	     return mav;
	 }
	 @RequestMapping("/delete")
	 public String deleteProduct(@RequestParam int id) {
	     ps.delete(id);
	     return "redirect:/";       
	 }

	 
}
