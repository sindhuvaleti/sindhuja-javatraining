package com.example.product;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class ProductService {
	
	 @Autowired 
	 private ProductRepository repo;
     
	    public void save(Product p) {
	        repo.save(p);
	    }
	     
	    public List<Product> listAll() {
	        return (List<Product>) repo.findAll();
	    }
	     
	    public Product get(int id) {
	        return repo.findById(id).get();
	    }
	     
	    public void delete(int id) {
	        repo.deleteById(id);
	    }

}
