package com.example.restfulwebservices.controller;

import com.example.restfulwebservices.service.NewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/getpublicapi")
public class NewController {
    @Autowired
    private NewService service;

    @GetMapping
    public String getApi(){

        return service.getPublicApi();
    }
}
