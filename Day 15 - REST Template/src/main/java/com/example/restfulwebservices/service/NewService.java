package com.example.restfulwebservices.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class NewService {

    public String getPublicApi(){
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<String> entity = new HttpEntity<String>(header);
        RestTemplate restTemp = new RestTemplate();
        return restTemp.exchange("https://api.nytimes.com/svc/topstories/v2/arts.json?api-key=UPxw9kG0O4f5AwWC94GvjIqTdOO3gokb", HttpMethod.GET, entity,String.class).getBody();
    }
}
