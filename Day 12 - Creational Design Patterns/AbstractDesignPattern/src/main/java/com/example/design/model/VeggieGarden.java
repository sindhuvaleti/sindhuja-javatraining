package com.example.design.model;

public class VeggieGarden extends Garden{

    @Override
    public Plant getCenter() {
        return new Plant("carrot");
    }

    @Override
    public Plant getShade() {
        return new Plant("peas");
    }

    @Override
    public Plant getBorder() {
        return new Plant("broccoli");
    }
}
