package com.example.design.model;

public class GardenMaker {
    private Garden gd;
    public Garden getGarden(String type){
        gd = new VeggieGarden();
        if(type.equals("perennial")){
            gd= new PerennialGarden();
        }
        if(type.equals("Annual")){
            gd= new AnnualGarden();
        }
        return gd;
    }
}
