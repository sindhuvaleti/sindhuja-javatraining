package com.example.design.model;

public class AnnualGarden extends Garden{
    @Override
    public Plant getCenter() {
        return new Plant("tomato");
    }

    @Override
    public Plant getShade() {
        return new Plant("potato");
    }

    @Override
    public Plant getBorder() {
        return new Plant("beans");
    }
}
