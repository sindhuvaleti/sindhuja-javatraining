package com.example.design.model;

public abstract class Garden {
    public abstract Plant getCenter();
    public abstract Plant getShade();
    public abstract Plant getBorder();

    @Override
    public String toString(){
        return String.format( "shade =" +this.getShade().getName() +
                " center="+this.getCenter().getName()+" border="+ this.getBorder().getName());
    }
}
