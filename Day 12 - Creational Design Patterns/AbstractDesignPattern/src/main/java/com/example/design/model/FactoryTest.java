package com.example.design.model;

public class FactoryTest {

    public static void main(String[] args){
        GardenMaker gm = new GardenMaker();
        System.out.println(gm.getGarden("Annual"));
        System.out.println(gm.getGarden("perennial"));
        System.out.println(gm.getGarden(""));

    }
}
