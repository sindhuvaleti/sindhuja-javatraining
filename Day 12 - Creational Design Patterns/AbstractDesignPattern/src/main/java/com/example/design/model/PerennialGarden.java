package com.example.design.model;

public class PerennialGarden extends Garden{
    @Override
    public Plant getCenter() {
        return new Plant("bellpepper");
    }

    @Override
    public Plant getShade() {
        return new Plant("cabbage");
    }

    @Override
    public Plant getBorder() {
        return new Plant("lettuce");
    }
}
