package com.example.design.model;

public class UserName {

    public String firstName, lastName;


    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }
    @Override
    public String toString(){
        return this.getFirstName() + this.getLastName();
    }
}
