package com.example.design.model;

public class LastNameFirst extends UserName {

    public LastNameFirst(String name){
        int i = name.indexOf(",");
        if(i>0){
            firstName = name.substring(i+1);
            lastName = name.substring(0,i);
        }
        else{
            firstName = "";
            lastName = name;
        }
    }
}
