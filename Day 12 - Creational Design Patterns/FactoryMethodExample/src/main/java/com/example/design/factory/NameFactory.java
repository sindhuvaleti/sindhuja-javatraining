package com.example.design.factory;

import com.example.design.model.FirstNameFirst;
import com.example.design.model.LastNameFirst;
import com.example.design.model.UserName;

public class NameFactory {

    public UserName NameFactory(String entry){
        if(entry.contains(",")){
            return new LastNameFirst(entry);
        }
            else{
                return new FirstNameFirst(entry);
            }
    }

}
