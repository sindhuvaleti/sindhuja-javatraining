package com.example.design.model;

public class FirstNameFirst extends UserName {

    public FirstNameFirst(String name){
        int i = name.lastIndexOf(" ");
        if(i>0){
            firstName = name.substring(0,i);
            lastName = name.substring(i+1);
        }
        else{
            firstName = "";
            lastName = name;
        }
    }
}
