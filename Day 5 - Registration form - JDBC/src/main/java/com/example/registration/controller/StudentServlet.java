package com.example.registration.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.registration.dao.StudentDao;
import com.example.registration.model.Student;

/**
 * Servlet implementation class StudentServlet
 */
@WebServlet("/register")
public class StudentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private StudentDao studentDao = new StudentDao();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public StudentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getRequestDispatcher("/WEB-INF/views/studentregister.jsp").include(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String firstName = request.getParameter("firstName");
		 String lastName = request.getParameter("lastName");
		 String username = request.getParameter("username");
		 String password = request.getParameter("password");
		 String address = request.getParameter("address");
		 String contact = request.getParameter("contact");

		 Student student = new Student();
		 student.setFirstName(firstName);
		 student.setLastName(lastName);
		 student.setUsername(username);
		 student.setPassword(password);
		 student.setContact(contact);
		 student.setAddress(address);

		 try {
		 studentDao.registerStudent(student);
		 } catch (Exception e) {
		 // TODO Auto-generated catch block
		 e.printStackTrace();
		 }

		 request.getRequestDispatcher("/WEB-INF/views/studentdetail.jsp").include(request, response);
		 }
		
	}


