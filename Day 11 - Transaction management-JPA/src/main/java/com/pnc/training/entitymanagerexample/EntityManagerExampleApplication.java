package com.pnc.training.entitymanagerexample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntityManagerExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntityManagerExampleApplication.class, args);
	}

}
