<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@page import="java.util.*" %>
    <%@page import="com.example.model.Customer" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Login Success</h1>
You logged in at
<%=request.getAttribute("localTime") %>
HitCount = <%=request.getSession().getAttribute("hitCount") %>
<br>
<table border="1">
<% List<Customer> cList =(List<Customer>) request.getAttribute("cList");
for(Customer c : cList){
	%>
	<tr><td><%=c.getId() %></td>  <td><%=c.getName() %></td></tr>
     <% 
}
%>

</table>
</body>
</html>