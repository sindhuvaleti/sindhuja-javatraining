package com.example.filters;

import java.io.IOException;
import java.time.LocalTime;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;

import com.example.model.User;
import com.test.db.UserManager;


/**
 * Servlet Filter implementation class LoginFilter
 */
@WebFilter(urlPatterns = { "/LoginServlet" }, servletNames = { "LoginServlet" })
public class LoginFilter implements Filter {

    /**
     * Default constructor. 
     */
    public LoginFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		// TODO Auto-generated method stub
		// place your code here
		String userName= (String)request.getParameter("userName");
		String password= (String)request.getParameter("password");
		User user=null;
		UserManager manager = new UserManager();
		try {
			 user= manager.getUser(userName);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(userName.equals(user.getId())&&password.equals(user.getPwd())) {
			
			
			
			request.setAttribute("localTime", LocalTime.now());
			// pass the request along the filter chain
			chain.doFilter(request, response);
			
			
		}
		else {
			request.getRequestDispatcher("error.jsp").include(request, response);
		}
		
		
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
