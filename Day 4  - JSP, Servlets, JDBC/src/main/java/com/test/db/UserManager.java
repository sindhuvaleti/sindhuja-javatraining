package com.test.db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import com.example.model.User;


public class UserManager {
	private String GET_USER = "select * from user where id=?";
	
	public User getUser(String id) throws Exception{
	
	Connection conn= ConnectionManager.getConnection();
	
	PreparedStatement ps = conn.prepareStatement(GET_USER);
	ps.setString(1, id);
	
	ResultSet rs= ps.executeQuery();
	User user=null;
	while(rs.next()) {
		user = new User(rs.getString(1), rs.getString(2));
	}
	return user;
	}

}
